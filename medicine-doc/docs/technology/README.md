### 后端
主要包含六个模块：
- **medicine-server：**[服务器端](../../../medicine-server)
- **medicine-collection：**[爬虫工程](../../../medicine-collection)
- **medicine-model：**[卷积神经网络](../../../medicine-model)
- **medicine-util：**[公用工具类](../../../medicine-util)
- **medicine-dataset：**[数据集](../../../medicine-dataset)
- **medicine-runtime-data：**[运行时数据](../../../medicine-runtime-data)

### App端
主要使用React Native + Ant Design 设计实现。

### 主要流程

<img src='../../assets/images/流程图.png' width='600px' />
