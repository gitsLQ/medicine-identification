
## 目录

* [前言](README.md)
* [产品功能](docs/function/README.md)
* [开发技术](docs/technology/README.md)
  * [数据集](docs/technology/DATASET.md)
  * [检测算法](docs/technology/ALGORITHM.md)
  * [App 端构建](docs/technology/APP.md)
  * [后端构建](docs/technology/SERVER.md)

